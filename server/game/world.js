var PlayerManager = require( './playerManager' )
var ProjectileManager = require( './projectileManager' )

var World = function( dims ) {
	this.dims = dims;
	this.offset = dims.divideNew( 2 );
	this.playerManager = new PlayerManager();
	this.projectileManager = new ProjectileManager();
	this.cells = [];
	for( var x = 0; x < dims.x; x++ ) {
		var col = []
		for( var y = 0; y < dims.y; y++ ) {
			var cell = new App.deadwood.CellData( new App.deadwood.Point(x-this.offset.x,y-this.offset.y), Math.random() );
			col.push( cell );
		}
		this.cells.push( col );
	}
}

// update /////////////////////////////////////////////////////////////////////
World.prototype.update = function( delta ) {
	this.projectileManager.update( delta );
}

// cells //////////////////////////////////////////////////////////////////////
World.prototype.getCell = function( x, y ) {
	var xpos = x + this.offset.x;
	var ypos = y + this.offset.y;

	var result = null;
	if( xpos >= 0 && xpos < this.dims.x && ypos >= 0 && ypos < this.dims.y ) {
		result = this.cells[ xpos ][ ypos ];
	}
	
	return result;
}

World.prototype.addObject = function( name, position ) {
	var cellId = App.deadwood.constants.World.getCellPosFromWorldPos( position );
	var cell = this.getCell( cellId.x, cellId.y );

	var objectData = new App.deadwood.CellData.ObjectData( name, position );
	cell.objects.push( objectData );

	var packet = new App.deadwood.packets.AddObject( objectData );
	App.socketio.emit( 'addobj', packet );
}

module.exports = function( dims ) {
	return new World( dims );
}