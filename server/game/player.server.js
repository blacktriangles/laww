
ServerPlayer = function( id, position, socket ) {
	this.socket = socket;
	this.data = new App.deadwood.packets.UpdatePlayer(
			id
		  , position
		  , false
		  , { x: 0, y: 0 }
		);
}

ServerPlayer.prototype.handleUpdatePacket = function( packet ) {
	if( this.data.id != packet.id ) {
		console.log( "Handling a packet that does not belong to us! " + this.data.id + " != " + packet.id );
		return;
	}

	this.data = packet;
}

module.exports = ServerPlayer;