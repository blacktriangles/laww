var World = require( './world' );

var Game = function() {
	this.world = new World( new App.deadwood.Point( 100, 100 ) );

	this.previousTick = Date.now();
	this.updateFreq = 45;
	this.tick = 0;
}

Game.prototype.begin = function() {
	App.game.loop();
}

Game.prototype.loop = function() {
	// using update loop method from
	// https://github.com/timetocode/net-engine-demo/blob/master/server/ServerEngine.js
	// described in
	// http://timetocode.tumblr.com/post/94896802141/netengine-source-code
	var now = Date.now();

	var test = App.game.previousTick + 1000 / App.game.updateFreq;
	if( test <= now ) {
		var delta = ( now - App.game.previousTick ) / 1000;
		App.game.previousTick = now;
		App.game.tick++;

		App.game.update( delta );
	}

	if( Date.now() - App.game.previousTick < 1000 / App.game.updateFreq - 16 ) {
		setTimeout( App.game.loop );
	}
	else {
		setImmediate( App.game.loop );
	}
}

Game.prototype.update = function( delta ) {
	App.game.world.update( delta );
} 

module.exports = function( app ) {
	return new Game();
}