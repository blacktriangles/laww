var ServerPlayer = require( './player.server' );

var PlayerManager = function() {
	this.players = {};
	this.lastId = 0;
}

PlayerManager.prototype.createPlayer = function( socket, position ) {
	var result = new ServerPlayer( this.lastId++, position, socket );
	this.players[ result.data.id ] = result;
	console.log( 'player added: ' + result.data.id + ' out of ' + this.toString() );
	return result;
}

PlayerManager.prototype.destroyPlayer = function( id ) {
	console.log( 'destroy player ' + id );
	this.players[ id ] = undefined;
}

PlayerManager.prototype.getPlayer = function( id ) {
	return this.players[id];
}

PlayerManager.prototype.getAllPlayersData = function( testFunc ) {
	var result = [];
	for( var id in this.players ) {
		var testPlayer = this.players[id];
		if( testFunc( testPlayer ) ) {
			if( this.players[id] !== null && this.players[id] !== undefined ) {
				result.push( this.players[id].data );
			}
		}
	}

	return result;
}

PlayerManager.prototype.toString = function() {
	var str = '[';
	for( var id in this.players ) {
		str += id + ',';
	}
	str += ']';
	return str;
}

module.exports = PlayerManager;