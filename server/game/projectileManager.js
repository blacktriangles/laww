var ServerProjectile = require( './projectile.server' );

var ProjectileManager = function() {
	this.projectiles = {};
	this.lastId = 0;
}

// update /////////////////////////////////////////////////////////////////////
ProjectileManager.prototype.update = function( delta ) {
	for( var id in this.projectiles ) {
		var projectile = this.projectiles[id];
		if( projectile != undefined ) {
			projectile.update();
			if( projectile.active == false ) {
				this.destroyProjectile( id );
			}
		}
	}
}

// public methods /////////////////////////////////////////////////////////////
ProjectileManager.prototype.createProjectile = function( data ) {
	data.id = this.lastId++;
	var result = new ServerProjectile( data );
	this.projectiles[ result.id ] = result;
	return result;
}

ProjectileManager.prototype.destroyProjectile = function( id ) {
	this.projectiles[ id ] = undefined;
	var destroypacket = new App.deadwood.packets.DestroyProjectile( id );
	App.socketio.emit( 'destroyproj', destroypacket );
}

ProjectileManager.prototype.getProjectile = function( id ) {
	return this.projectiles[id];
}

module.exports = ProjectileManager;