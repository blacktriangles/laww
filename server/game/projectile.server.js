
var ServerProjectile = function( data ) {
	this.id = data.id;
	this.data = data;
	this.data.position = new App.deadwood.Point( this.data.position.x, this.data.position.y );
	this.data.dir = new App.deadwood.Point( this.data.dir.x, this.data.dir.y );
	this.deltaMove = this.data.dir.multiplyNew( this.data.speed );
	this.active = true;
}

ServerProjectile.prototype.update = function( delta ) {
	if( this.data.range -- <= 0 ) {
		console.log( 'range' );
		this.active = false;
	}
	else {
		for( var id in App.game.world.playerManager.players ) {
			var player = App.game.world.playerManager.players[id];
			if( player !== null && player !== undefined && player.data !== null && player.data !== undefined ) {
				if( player.data.id !== this.data.shooterId ) {
					this.data.position.plusEq( this.deltaMove );

					var dist2 = App.deadwood.Point.distanceSquared( player.data.position, this.data.position );

					if( dist2 < this.data.hitRadiusSqr ) {
						var hitpacket = new App.deadwood.packets.ProjectileHit( this.id, player.data.id );	
						App.socketio.emit( 'projhit', hitpacket );
						this.active = false;

						console.log( 'creating corpse' );
						App.game.world.addObject( 'corpse', player.data.position );
						return;
					}
				}
			}
		}
	}
}

module.exports = ServerProjectile;