var env						= process.env.NODE_ENV || 'development'
,  package					= require( './package.json' )
,  path						= require( 'path' )
,  express					= require( 'express' )
,  http						= require( 'http' )
,  bodyParser				= require( 'body-parser' )
,  socketio					= require( 'socket.io' )
,  rootDir					= __dirname

global.App = {
	express: express()
  , passport: null
  , server: null
  , socketio: null
  , port: process.env.PORT || 9999
  , ioport: process.env.IOPORT || 9900
  , package: package
  , version: package.version
  , env: env
  , routes: require( './routes/routes' )
  , dirs: {
  		root: rootDir
  	  , client: path.join( rootDir, '../client/' )
	  , common: path.join( rootDir, '../common/' )
	}
  , deadwood: {}
  , game: {}
  
  , start: function( addRoutes ) {
		App.loadCommon();
		App.game = require( './game/game' )();

		App.server = http.createServer( App.express );
		App.socketio = socketio( App.server );

		App.express.use( bodyParser() );
		App.routes()
		if( addRoutes !== undefined ) {
			addRoutes()
		}

		App.server.listen( App.port );

		console.log( 'Started ' + App.package.name + ' in ' + App.env + ' mode on port ' + App.port );

		App.game.begin();
	}

  , loadCommon: function() {
		App.deadwood.constants = require( path.join( App.dirs.common, '/js/constants' ) );
		App.deadwood.Point = require( path.join( App.dirs.common, 'lib/vector2' ) );
		App.deadwood.CellData = require( path.join( App.dirs.common, '/js/world/cellData' ) );
		App.deadwood.Cell = require( path.join( App.dirs.common, '/js/world/cell' ) );
		App.deadwood.packets = require( path.join( App.dirs.common, '/js/packets/packets' ) );
	}
}