
module.exports = function() {
	App.socketio.on( 'connection', function( socket ) {
		console.log( "New player connected " + socket.id );
		var pixelPos = App.deadwood.constants.Cell.randomPixelInCellGlobal( new App.deadwood.Point( 0, 0 ) );

		var playerList = App.game.world.playerManager.getAllPlayersData( function( player ) {
			return true;
		});

		var newPlayer = App.game.world.playerManager.createPlayer( socket, pixelPos )

		var enterGamePacket = new App.deadwood.packets.EnterGame( newPlayer.data, playerList ); 
		var newPlayerPacket = new App.deadwood.packets.NewPlayer( newPlayer.data );

		socket.emit( 'entergame', enterGamePacket );
		socket.broadcast.emit( 'newplayer', newPlayerPacket );

		socket.on( 'disconnect', function() {
			console.log( 'disconnected ' + socket.id );
			App.game.world.playerManager.destroyPlayer( newPlayer.data.id );
			socket.broadcast.emit( 'playerleft', newPlayer.data.id );
		});

		socket.on( 'updateplayer', function( data ) {
			///##hsmith $TODO only send to interested clients
			var player = App.game.world.playerManager.getPlayer( data.id );
			if( player === undefined || player === null ) 
			{
				console.log( 'cannot find player ' + data.id );
				for( var key in App.game.world.playerManager.players ) {
					console.log( '\t' + key );
				}
			}
			else {
				player.handleUpdatePacket( data );
				socket.broadcast.emit( 'updateplayer', data );
			}
		});

		socket.on( 'spawnproj', function( data ) {
			var proj = App.game.world.projectileManager.createProjectile( data );
			App.socketio.emit( 'spawnproj', proj.data );
		});
	})
}