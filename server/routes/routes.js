
module.exports = function() {
	// http routes //
	require( './http/static' )();
	require( './http/celldata' )();

	// socketio routes //
	require( './socket/connected' )();
}