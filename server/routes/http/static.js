var express					= require( 'express' )
  , request					= require( 'request' )

module.exports = function() {
	App.express.use( express.static( App.dirs.client ) );
	App.express.use( '/common', express.static( App.dirs.common ) );
	App.express.get( '/gdoc', function( req, res ) {
		console.log( 'getting gdoc' );
		request( { 
					url: 'https://docs.google.com/spreadsheets/d/1ZVghR63myhxAHj2xIuFAATTqPMtwLSlMe-NP3fELnW8/pubhtml'
				  , json: true
				}, function( error, reqres, body ) {
					res.json( body );
			});
	})
}