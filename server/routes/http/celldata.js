var express					= require( 'express' )

module.exports = function() {
	App.express.get( '/data/cell/:x/:y', function( req, res ) {
		var cell = App.game.world.getCell( parseInt(req.params.x), parseInt(req.params.y) );
		if( cell !== null ) {
			res.json( cell );
		}
		else {
			res.json( { seed: 'invalid' } );
		}
	});
}