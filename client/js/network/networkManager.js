
Deadwood.NetworkManager = function() {
	this.socket = null;
}

Deadwood.NetworkManager.prototype.connect = function() {
	this.socket = io();
	this.socket.on( 'entergame', this.onEnterGame );
	this.socket.on( 'newplayer', this.onNewPlayer );
	this.socket.on( 'playerleft', this.onPlayerLeft );
	this.socket.on( 'updateplayer', this.onUpdatePlayer );
	this.socket.on( 'spawnproj', this.onSpawnProjectile );
	this.socket.on( 'destroyproj', this.onDestroyProjectile );
	this.socket.on( 'projhit', this.onProjectileHit );
	this.socket.on( 'addobj', this.onAddObject );

	// local update variables //
	this.counter = 0;
	this.lastUpdatePacket = null;
}

// update /////////////////////////////////////////////////////////////////////
Deadwood.NetworkManager.prototype.update = function() {
	if( Deadwood.game.localPlayer === null || Deadwood.game.localPlayer === undefined ) return;
	if( Deadwood.game.localPlayer.isDead ) return;
	if( this.counter++ >= Deadwood.NetworkManager.updateFrequency ) {
		this.counter = 0;

		var player = Deadwood.game.localPlayer;
		var packet = new Deadwood.Packets.UpdatePlayer( 
				player.id
			  , player.sprite.position.clone()
			  , player.aimPos.clone()
			  , player.selectedWeapon
			);

		if( packet.equals( this.lastUpdatePacket ) == false ) {
			this.socket.emit( 'updateplayer', packet );
			this.lastUpdatePacket = packet;
		}
	}
}

// public methods /////////////////////////////////////////////////////////////
Deadwood.NetworkManager.prototype.spawnProjectile = function( packet ) {
	this.socket.emit( 'spawnproj', packet );
}

// network callbacks //////////////////////////////////////////////////////////
Deadwood.NetworkManager.prototype.onEnterGame = function( enterGamePacket ) {
	var localPos = new PIXI.Point( enterGamePacket.localPlayer.position.x, enterGamePacket.localPlayer.position.y );
	if( Deadwood.game.localPlayer == null ) {
	    Deadwood.game.createLocalPlayer( enterGamePacket.localPlayer.id, localPos );
	}
	else {
		Deadwood.game.localPlayer.sprite.position = localPos;
		Deadwood.game.localPlayer.id = enterGamePacket.localPlayer.id;
	}

	for( var i = 0; i < enterGamePacket.remotePlayers.length; i++ ) {
		var playerdat = enterGamePacket.remotePlayers[i];
		if( playerdat.id !== Deadwood.game.localPlayer.id ) {
			Deadwood.networkManager.onRemotePlayer( playerdat );
		}
	}
}

Deadwood.NetworkManager.prototype.onNewPlayer = function( packet ) {
	if( packet.data.id != Deadwood.game.localPlayer.id ) {
		Deadwood.networkManager.onRemotePlayer( packet.data );
	}
}

Deadwood.NetworkManager.prototype.onPlayerLeft = function( id ) {
	if( id != Deadwood.game.localPlayer.id ) {
		Deadwood.game.world.removePlayer( id );
	}
}

Deadwood.NetworkManager.prototype.onRemotePlayer = function( playerdat ) {
	if( playerdat.id != Deadwood.game.localPlayer.id ) {
		playerdat.position = new PIXI.Point( playerdat.position.x, playerdat.position.y );
		if( Deadwood.game.world.getPlayer( playerdat.id ) == null ) {
			Deadwood.game.createRemotePlayer( playerdat );
		}
	}
}

Deadwood.NetworkManager.prototype.onUpdatePlayer = function( packet ) {
	var player = Deadwood.game.world.getPlayer( packet.id );
	player.handleUpdatePacket( packet );
}

Deadwood.NetworkManager.prototype.onSpawnProjectile = function( packet ) {
	console.log( 'create proj' );
	var proj = new Deadwood.Projectile( packet );
	Deadwood.game.addProjectile( proj );
}

Deadwood.NetworkManager.prototype.onDestroyProjectile = function( packet ) {
	console.log( 'destroy proj' );
	Deadwood.game.removeProjectile( packet.id );
}

Deadwood.NetworkManager.prototype.onProjectileHit = function( packet ) {
	console.log( 'hit!' );
	Deadwood.game.removeProjectile( packet.id );

	if( packet.playerId == Deadwood.game.localPlayer.id ) {
		Deadwood.game.localPlayer.onHit();
	}
	else {
		var player = Deadwood.game.world.getPlayer( packet.playerId );
		if( player !== null && player !== undefined ) {
			player.onHit();
		}
	}
}

Deadwood.NetworkManager.prototype.onAddObject = function( packet ) {
	console.log( 'add object' );
	packet.data.position = new PIXI.Point( packet.data.position.x, packet.data.position.y );
	var cellId = Deadwood.World.getCellPosFromWorldPos( packet.data.position );
	
	var cell = Deadwood.game.world.getCell( cellId );
	if( cell !== undefined && cell !== null ) {
		cell.addObject( packet.data );
	}
}

Deadwood.networkManager = new Deadwood.NetworkManager();