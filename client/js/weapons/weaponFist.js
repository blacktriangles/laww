
Deadwood.WeaponFist = function( owner ) {
	Deadwood.Weapon.call( this, 'img/characters/cowboy/fist.png' );
	this.owner = owner;
}

Deadwood.WeaponFist.prototype = Object.create( Deadwood.Weapon.prototype );

Deadwood.WeaponFist.prototype.trigger = function() {
	for( var i = 0; i < Deadwood.game.children.length; i++ ) {
		var child = Deadwood.game.children[i];
		if( child.interact !== undefined 
		  && child.isInteractable
		  && child.isHovering
		  && child.isInRange
		)
		{
			child.interact( this.owner );
		}
	}
}