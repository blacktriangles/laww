
Deadwood.WeaponRevolver = function( owner ) {
	Deadwood.Weapon.call( this, 'img/characters/cowboy/revolver.png' );

	this.owner = owner;
	this.clipSize = 6;
	this.rounds = [true,true,true,true,true,true];
	this.cocked = false;
    this.bulletSprites = [];
}

Deadwood.WeaponRevolver.prototype = Object.create( Deadwood.Weapon.prototype );

Deadwood.WeaponRevolver.prototype.update = function() {
}

Deadwood.WeaponRevolver.prototype.trigger = function() {
	if( this.cocked ) {
		this.cocked = false;
		if( this.rounds[0] ) {
            this.rounds[0] = false;
            this.fire();
		}
		else {
			this.dryFire();
		}
        var first = this.rounds[0];
        this.rounds.shift();
        this.rounds.push(first);
	}
	else {
		console.log( 'not cocked!' );
	}
}

Deadwood.WeaponRevolver.prototype.fire = function() {
	console.log( 'fire' );
	var toCursor = this.owner.aimPos.minusNew( this.owner.sprite.position );
	var packet = new Deadwood.Packets.SpawnProjectile( 'img/projectiles/bullet.png'
											, this.owner.id
											, toCursor.normalise().clone()
											, this.owner.sprite.position.clone()
											, 5 	// speed
											, 50 	// range
											, 20	// hitRadius
										);

	Deadwood.networkManager.spawnProjectile( packet );
}

Deadwood.WeaponRevolver.prototype.dryFire = function() {
	console.log( 'click' );
}

Deadwood.WeaponRevolver.prototype.cock = function() {
	if( this.cocked ) {
		console.log( 'unclick' );
	}
	else {
		console.log( 'chick chick' );
	}

	this.cocked = !this.cocked;
}

Deadwood.WeaponRevolver.prototype.reload = function() {
    this.rounds = [true,true,true,true,true,true];
}

Deadwood.WeaponRevolver.prototype.showReloadOverlay = function() {
    var dim = Deadwood.World.stageSize;
    if(this.reloadingSprite == null){
        this.reloadingSprite = PIXI.Sprite.createSprite( 'img/ui/UI_ReloadPistol_Base.png',new Vector2(48,dim.y-45));
        this.reloadingSprite.anchor.x = 0.5;
        this.reloadingSprite.anchor.y = 0.5;
    }
    Deadwood.game.renderLayers[RenderLayer.DIALOG].addChild(this.reloadingSprite);
    for(var i = 0 ; i < this.clipSize; i++){
        var r = 27;
        var p = new Vector2(48-Math.sin(i*2*Math.PI/6-Math.PI)*r+1,dim.y-45+Math.cos(i*2*Math.PI/6-Math.PI)*r-1)
        var bullet = PIXI.Sprite.createSprite( 'img/ui/UI_ReloadPistol_Bullet.png',p);
        bullet.anchor.x = 0.5;
        bullet.anchor.y = 0.5;
        this.bulletSprites.push(bullet)
    }
    this.updateRounds();r
}

Deadwood.WeaponRevolver.prototype.updateRounds = function() {
    for(var i = 0 ; i < this.clipSize; i++){
        if(this.rounds[i]){
            if(!this.bulletSprites[i].parent){
                Deadwood.game.renderLayers[RenderLayer.DIALOG].addChild(this.bulletSprites[i]);
            }
        }
        else {
            if(this.bulletSprites[i].parent){
                Deadwood.game.renderLayers[RenderLayer.DIALOG].removeChild(this.bulletSprites[i]);
            }
        }
    }
}

Deadwood.WeaponRevolver.prototype.hideReloadOverlay = function() {
    Deadwood.game.renderLayers[RenderLayer.DIALOG].removeChild(this.reloadingSprite);
    for(var i = 0 ; i < this.clipSize; i++){
        if(this.bulletSprites[i].parent) {
            Deadwood.game.renderLayers[RenderLayer.DIALOG].removeChild(this.bulletSprites[i]);
        }
    }
}

Deadwood.WeaponRevolver.prototype.reloadClick = function( ev ) {
    var dim = Deadwood.World.stageSize;
    for(var i = 0 ; i < this.rounds.length; i++){
        var r = 27;
        var p = new Vector2(48-Math.sin(i*2*Math.PI/6-Math.PI)*r+1,dim.y-45+Math.cos(i*2*Math.PI/6-Math.PI)*r-1)
        var toCursor = Deadwood.game.mousePos.minusNew( p );
        if(toCursor.magnitude()<15){
            this.rounds[i] = true;
        }
    }

    this.updateRounds();
}
