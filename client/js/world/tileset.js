
Deadwood.Tileset = {};

Deadwood.Tileset.load = function( callback ) {
	$.getJSON('common/data/tileset.json', function( json ) {
		Deadwood.tileset = json;
		for( var type in Deadwood.tileset ) {
			var typeObj = Deadwood.tileset[type];
			var list =[];
			var count = 0;
			for( var key in typeObj ) {
				var entry = typeObj[key];
				entry.texture = Deadwood.resourceManager.getTexture( entry.src );
				entry.id = count;
				entry.name = key;
				list[count++] = entry;
			}

			typeObj.list = list;

			typeObj.getRandomIndex = function() {
				return Math.floor( Math.random() * (this.list.length) );
			}

			typeObj.getRandom = function() {
				var index = this.getRandomIndex();
				return this.list[index];
			}
		}
		callback();
	});
}