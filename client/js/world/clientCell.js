
Deadwood.ClientCell = function( world, cellPosition ) {
	Deadwood.Cell.call( this, cellPosition );

	this.cellPos = cellPosition.clone();
	this.tiles = [];
	this.objects = [];
	this.players = [];

	var _this = this;
	world.getCellData( this.cellPos, function( cellData ) {
		if( cellData.seed == "invalid" ) {
			console.log( 'invalid seed' );
		}
		else {
			_this.data = cellData;
			Math.seedrandom( cellData.seed );
			for( var y = 0; y < Deadwood.Cell.cellDims.y; y++ ) {
				var row = [];
				for( var x = 0; x < Deadwood.Cell.cellDims.x; x++ ) {
					var texture = Deadwood.tileset['ground'].getRandom().texture;
					var tile = new Deadwood.Tile( texture, Deadwood.Cell.localTileToWorldPos( _this.cellPos, new PIXI.Point(x,y) ) );
					row.push( tile );
					Deadwood.game.renderLayers[RenderLayer.FLOOR].addChild( tile.sprite );
				}

				_this.tiles.push( row );
			}

			for( var i = 0; i < _this.data.objects.length; i++ ) {
				var objDat = _this.data.objects[i];
				_this.addObject( objDat );
			}

			Math.seedrandom();
		}
	});
}

Deadwood.ClientCell.prototype = Object.create( Deadwood.Cell.prototype );

Deadwood.ClientCell.prototype.addObject = function( objDat ) {
	var newGo = Deadwood.GameObject.create( objDat.id, new PIXI.Point( objDat.position.x, objDat.position.y ) );
	this.objects.push( newGo );
	Deadwood.game.add( newGo );
}

Deadwood.ClientCell.prototype.destroy = function() {
	for( var y = 0; y < Deadwood.Cell.cellDims.y; y++ ) {
		for( var x = 0; x < Deadwood.Cell.cellDims.x; x++ ) {
			var tile = this.getTile( x, y );
			if( tile !== null ) {
				Deadwood.game.renderLayers[RenderLayer.FLOOR].removeChild( tile.sprite );
			}
		}
	}

	for( var i = 0; i < this.objects.length; i++ ) {
		this.objects[i].destroy();
	}
}

Deadwood.ClientCell.prototype.addGameObject = function( gameObject ) {
	this.objects.push( gameObject );
}

Deadwood.ClientCell.prototype.removeGameObject = function( gameObject ) {
	var result = false;
	var index = this.objects.indexOf( gameObject );
	if( index >= 0 ) {
		this.objects.splice( index, 1 );
		result = true;
	}

	return result;
}

Deadwood.ClientCell.prototype.getTile = function( x, y ) {
	if( x < this.tiles.length && x >= 0 ) {
		if( x < this.tiles[x].length && y >= 0 ) {
			return this.tiles[x][y];
		}
	}

	return null;
}