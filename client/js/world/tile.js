
Deadwood.Tile = function( texture, pos ) {
	this.renderLayer = RenderLayer.FLOOR;
	this.sprite = new PIXI.Sprite( texture );
	this.sprite.position = pos;
}

Deadwood.Tile.prototype.update =function() {
	this.sprite.tint = 0xffffff; //Deadwood.game.world.timeColor.hexString();
	console.log( Deadwood.game.world.timeColor.hexString() );
}