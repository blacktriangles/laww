
Deadwood.World = function() {
	var _this = this;
	this.players = {};
	this.cellPos = new PIXI.Point( 0, 0 );
	this.cells = [];
	for( var x = 0; x < 3; x++ ) {
		this.cells[x] = [];
		for( var y = 0; y < 3; y++ ) {
			var cell = new Deadwood.ClientCell( this, new PIXI.Point( x-1, y-1 ) );
			this.cells[x][y] = cell;
		}
	}
}

// methods ////////////////////////////////////////////////////////////////////
Deadwood.World.prototype.update = function() {
}

Deadwood.World.prototype.addPlayer = function( player ) {
	this.players[player.id] = player;
}

Deadwood.World.prototype.removePlayer = function( id ) {
	var player = this.players[id];
	if( player !== undefined ) {
		Deadwood.game.remove( player );
		this.players[id] = undefined;
	}
}

Deadwood.World.prototype.getPlayer = function( id ) {
	return this.players[id];
}

Deadwood.World.prototype.getCellData = function( cellPos, callback ) {
	///###hsmith $TEMP spoof data
	$.getJSON( '/data/cell/'+cellPos.x+'/'+cellPos.y, function( celldata ) {
		callback( celldata );
	});
}

Deadwood.World.prototype.updateCellPos = function( cellPos ) {
	if( cellPos.x < this.cellPos.x ) {
		this.shiftCellsRight();
	}
	else if( cellPos.x > this.cellPos.x ) {
		this.shiftCellsLeft();
	}

	if( cellPos.y > this.cellPos.y ) {
		this.shiftCellsUp();
	}
	else if( cellPos.y < this.cellPos.y ) {
		this.shiftCellsDown();
	}

	this.cellPos = cellPos;
}

Deadwood.World.prototype.changeCell = function( gameObject, oldPos, newPos ) {
	if( oldPos !== null && oldPos !== undefined ) {
		var oldCell = this.getCell( oldPos );
		if( oldCell !== null ) {
			oldCell.removeGameObject( gameObject );
		}
	}

	var newCell = this.getCell( newPos );
	if( newCell !== null ) {
		newCell.addGameObject( gameObject );
	}
	else {
		gameObject.destroy();
	}
}

Deadwood.World.prototype.getCell = function( cellPos ) {
	var relCel = cellPos.minusNew( this.cellPos ).plusEq( new PIXI.Point( 1, 1 ) );
	if( relCel.x < 0 || relCel.x > 2 ) return null;
	if( relCel.y < 0 || relCel.y > 2 ) return null;

	return this.cells[ relCel.x ][ relCel.y ];
}

// internal methods ///////////////////////////////////////////////////////////
Deadwood.World.prototype.cloneCellRow = function( x ) {
	var cloneRow = [];
	for( var y = 0; y < 3; y++ ) {
		cloneRow[y] = this.cells[x][y].clone();
	}
}

Deadwood.World.prototype.shiftCellsRight = function() {
	var deadCells = this.cells[2];
	this.cells[2] = this.cells[1];
	this.cells[1] = this.cells[0];
	this.cells[0] = [];

	for( var y = 0; y < 3; y++ ) {
		// destroy old cells
		deadCells[y].destroy();

		var newCellPos = this.cellPos.clone();	
		newCellPos.x -= 2;
		newCellPos.y += y-1;
		this.cells[0][y] = new Deadwood.ClientCell( this, newCellPos );
	}
}

Deadwood.World.prototype.shiftCellsLeft = function() {
	var deadCells = this.cells[0];
	this.cells[0] = this.cells[1];
	this.cells[1] = this.cells[2];
	this.cells[2] = [];

	for( var y = 0; y < 3; y++ ) {
		deadCells[y].destroy();

		var newCellPos = this.cellPos.clone();	
		newCellPos.x += 2;
		newCellPos.y += y-1;
		this.cells[2][y] = new Deadwood.ClientCell( this, newCellPos );
	}	
}

Deadwood.World.prototype.shiftCellsUp = function() {
	for( var x = 0; x < 3; x++ ) {
		this.cells[x][0].destroy();
		this.cells[x][0] = this.cells[x][1];
		this.cells[x][1] = this.cells[x][2];

		var newCellPos = this.cellPos.clone();
		newCellPos.x += (x-1);
		newCellPos.y += 2;
		this.cells[x][2] = new Deadwood.ClientCell( this, newCellPos );
	}
}

Deadwood.World.prototype.shiftCellsDown = function() {
	for( var x = 0; x < 3; x++ ) {
		this.cells[x][2].destroy();
		this.cells[x][2] = this.cells[x][1];
		this.cells[x][1] = this.cells[x][0];

		var newCellPos = this.cellPos.clone();
		newCellPos.x += (x-1);
		newCellPos.y -= 2;
		this.cells[x][0] = new Deadwood.ClientCell( this, newCellPos );
	}
}