
Deadwood.Cell = function( pos ) {
	this.data = null;
	this.worldTilePos = this.data.cellPosition.multiplyNewVec( Deadwood.Cell.cellDims );
	this.worldPos = Deadwood.World.tileToPixelPos( this.worldTilePos );
}

Deadwood.Cell.prototype.localToWorldTilePos = function( localTilePos ) {
	return localTilePos.plusNew( this.worldTilePos );
}

Deadwood.Cell.prototype.localToWorldPos = function( localPixelPos ) {
	return localPixelPos.plusNew( this.worldPos )
}

Deadwood.Cell.prototype.localTileToWorldPos = function( localTilePos ) {
	var worldTilePos = this.localToWorldTilePos( localTilePos );
	return Deadwood.World.tileToPixelPos( worldTilePos );
}

Deadwood.Cell.prototype.randomCellPos = function() {
	var posx = Math.random() * Deadwood.Cell.pixelDims.x;
	var posy = Math.random() * Deadwood.Cell.pixelDims.y;
	var result = (new PIXI.Point( posx, posy ));
	return result;
}

Deadwood.Cell.prototype.randomPos = function() {
	var result = this.randomCellPos();
	return this.localToWorldPos( result );
}

Deadwood.Cell.prototype.randomCellTilePos = function() {
	var posx = Math.floor( Math.random() * Deadwood.Cell.cellDims.x );
	var posy = Math.floor( Math.random() * Deadwood.Cell.cellDims.y );
	var result = (new PIXI.Point( posx, posy )).plusEq( this.position );
	return result;	
}

Deadwood.Cell.prototype.randomTilePos = function() {
	var result = this.randomCellTilePos();
	return this.localToWorldTilePos( result );
}

module.exports = Deadwood.Cell;