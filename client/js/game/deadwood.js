var Deadwood = {};

Math.seedrandom();

Deadwood.Point = Vector2;
PIXI.Point = Vector2;

// extensions /////////////////////////////////////////////////////////////////
PIXI.Sprite.prototype.isMouseOver = function() {
       return !( Deadwood.game.mousePos.x < this.position.x ||
                               Deadwood.game.mousePos.x > this.position.x + Math.abs(this.width) ||
                               Deadwood.game.mousePos.y < this.position.y &&
                               Deadwood.game.mousePos.y > this.position.y + Math.abs(this.height) );
}

Math.lerp = function(a, b, d) {
    return (1 - d) * a + d * b;
};

Color.lerp = function( a, b, d ) {
	return new Color(
		{
			r: Math.lerp( a.red(), b.red(), d )
		  , g: Math.lerp( a.green(), b.green(), d )
		  , b: Math.lerp( a.blue(), b.blue(), d )
		}
	);
};