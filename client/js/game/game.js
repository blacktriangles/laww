
var RenderLayer = {
	BACKGROUND: 	0
  , FLOOR: 			1
  , PROP: 			2
  , PLAYER: 		3
  , PROJECTILE: 	4
  , OVERLAY: 		5
  , GUI: 			6
  , DIALOG: 		7
  , CURSOR: 		8
}

Deadwood.Game = function() {
	this.localPlayer = null;
    this.children = [];
	this.projectiles = {};
	this.renderLayers = [];
	this.mousePos = new PIXI.Point( 0, 0 );
	this.deltaMouse = new PIXI.Point( 0, 0 );

	this.sprite = null;
	this.renderLayers = {};
	this.world = null;

	this.keyStates = {
		getKeyState: function( keycode ) {
			var result = this[keycode];
			if( result === undefined ) {
				result = false;
			}
			return result;
		}
	};

    this.mouseStates = {
        leftButton: false,
        rightButton: false
    }
}

Deadwood.Game.prototype.start = function() {
	$('#debug').append( '<p id="debug_mousepos">asdf</p>' );
	this.stage = new PIXI.Stage();
	this.sprite = new PIXI.DisplayObjectContainer();
	this.scaler = new PIXI.DisplayObjectContainer();
	this.scaler.scale.x = Deadwood.World.scaleFactor.x;
	this.scaler.scale.y = Deadwood.World.scaleFactor.y;
	this.scaler.addChild( this.sprite );
	this.stage.addChild( this.scaler );

	this.renderLayers = {};
	for( var layerName in RenderLayer ) {
		var layer = new PIXI.DisplayObjectContainer();
		var index = RenderLayer[layerName];
		this.renderLayers[index] = layer;
		if( index == RenderLayer.GUI || index == RenderLayer.DIALOG || index == RenderLayer.CURSOR ) {
			this.scaler.addChild( layer ); //gui layer does not get scrolled
		}
		else {
			this.sprite.addChild( layer );
		}
	}

	this.renderer = new PIXI.autoDetectRenderer( Deadwood.World.stageSize.x * Deadwood.World.scaleFactor.x, Deadwood.World.stageSize.y * Deadwood.World.scaleFactor.y );

	this.renderer.view.id = 'view';
	$('#stage').append( this.renderer.view );

	var _this = this;
	var animate = function() {
		requestAnimFrame( animate );
		_this.update();
		_this.renderer.render( _this.stage );
	}

	requestAnimFrame( animate );

    $('#view').on( 'mouseenter', function( ev ) {
        if(_this.cursor) {
            _this.cursor.sprite.alpha = 1;
        }
    });

    $('#view').on( 'mouseleave', function( ev ) {
        if(_this.cursor) {
            _this.cursor.sprite.alpha = 0;
        }
    });

    $('#view').on( 'mousemove', function( ev ) {
		_this.onMouseMove( ev );
	});

    $('#view').on( 'mousedown', function( ev ) {
        if(ev.button == 0){
            _this.mouseStates.leftButton = true;
        }
        else if(ev.button == 2) {
            _this.mouseStates.rightButton = true;
        }

		ev.preventDefault();
    });

    $('#view').on( 'mouseup', function( ev ) {
        if(ev.button == 0){
            _this.mouseStates.leftButton = false;
        }
        else if(ev.button == 2) {
            _this.mouseStates.rightButton = false;
        }
    });

	$(window).keydown( function( ev ) {
		_this.onKeyDown( ev );
	});

	$(window).keyup( function( ev ) {
		_this.onKeyUp( ev );
	});

	Deadwood.networkManager.connect();
}

Deadwood.Game.prototype.createLocalPlayer = function( id, pos ) {
	this.localPlayer = new Deadwood.LocalPlayer( id, pos );
	this.sprite.position.x = pos.x + Deadwood.World.centerStage.x;
	this.sprite.position.y = pos.y + Deadwood.World.centerStage.y;
	this.add( this.localPlayer );

	this.world = new Deadwood.World();
	this.world.addPlayer( this.localPlayer );
}

Deadwood.Game.prototype.createRemotePlayer = function( playerdat ) {
	playerdat.position = new PIXI.Point( playerdat.position.x, playerdat.position.y );

	if( this.world.getPlayer( playerdat.id ) === undefined ) {
		console.log( 'adding a new remote player: ' + playerdat.id );
		var remotePlayer = new Deadwood.RemotePlayer( playerdat.id, playerdat.position );
		this.add( remotePlayer );
		this.world.addPlayer( remotePlayer );
	}
	else {
		console.log( 'already had an id? ' + playerdat.id );
	}
}

Deadwood.Game.prototype.add = function(gameObject, layerIndex ){
	if( layerIndex !== undefined ) {
		gameObject.renderLayer = layerIndex;
	}

    this.children.push(gameObject);
    this.renderLayers[gameObject.renderLayer].addChild( gameObject.sprite );
}

Deadwood.Game.prototype.remove = function(gameObject){
	var index = this.children.indexOf(gameObject);
	this.children.splice( index, 1 );
	this.renderLayers[gameObject.renderLayer].removeChild( gameObject.sprite );
	gameObject.onDestroy();
}

Deadwood.Game.prototype.addProjectile = function( projectile ) {
	projectile.renderLayer = RenderLayer.PROJECTILE;
	this.projectiles[projectile.id] = projectile;
	this.renderLayers[RenderLayer.PROJECTILE].addChild( projectile.sprite );
}

Deadwood.Game.prototype.removeProjectile = function( id ) {
	var projectile = this.projectiles[id];
	if( projectile !== null && projectile !== undefined ) {
		this.renderLayers[RenderLayer.PROJECTILE].removeChild( projectile.sprite );
		this.projectiles[id] = undefined;
		projectile.onDestroy();
	}
}

Deadwood.Game.prototype.update = function() {
	if( this.world !== null ) {
		this.world.update();
	}

	for(var i = 0 ; i  < this.children.length; i++){
        this.children[i].update();
    }

	for( var id in this.projectiles ) {
		var projectile = this.projectiles[id];
		if( projectile !== null && projectile !== undefined ) {
			projectile.update();
		}
	}

	Deadwood.networkManager.update();
}

Deadwood.Game.prototype.onMouseMove = function( ev ) {
	var pos = $('#view').offset()

	var lastMouse = Deadwood.game.mousePos;

	var rawx = ev.pageX - pos.left;
	var rawy = ev.pageY - pos.top;

	Deadwood.game.mousePos = new PIXI.Point( 
		rawx / Deadwood.World.scaleFactor.x
	  , rawy / Deadwood.World.scaleFactor.y
	);

	Deadwood.game.deltaMouse = Deadwood.game.mousePos.minusNew( lastMouse );
	$('#debug_mousepos').html( '\n' + this.sprite.position.x + ',' + this.sprite.position.y );
};

Deadwood.Game.prototype.onKeyDown = function( ev ) {
	this.keyStates[ ev.which ] = true;
}

Deadwood.Game.prototype.onKeyUp = function( ev ) {
	this.keyStates[ ev.which ] = false;
}

Deadwood.game = new Deadwood.Game();

$( document ).ready( function() {
	Deadwood.Tileset.load( function() {
		Deadwood.game.start();
		$('#view').bind('contextmenu', function() {
			return false;
		});
	})
});
