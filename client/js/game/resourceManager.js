
Deadwood.ResourceManager = function() {
	this.textures = {};
}

Deadwood.ResourceManager.prototype.getTexture = function( path ) {
	if( this.textures[ path ] == null ) {
		this.textures[ path ] = PIXI.Texture.fromImage( path, false, PIXI.scaleModes.NEAREST );
	}

	return this.textures[ path ];
}

Deadwood.resourceManager = new Deadwood.ResourceManager();

PIXI.Sprite.prototype.changeTexture = function( path ) {
	var texture = Deadwood.resourceManager.getTexture( path );
	this.setTexture( texture );
}

PIXI.Sprite.createSprite = function( path, position ) {
	var texture = Deadwood.resourceManager.getTexture( path );
	var result = new PIXI.Sprite( texture );
	result.position = position.clone();
	return result;
}