
Deadwood.Coyote = function( position ) {
	Deadwood.GameObject.call( this, position );

	var skinDir = 'img/characters/coyote';
	this.idleTexture = Deadwood.resourceManager.getTexture( skinDir + '/coyote_standing.png' );
	this.runTextures = [
		Deadwood.resourceManager.getTexture( skinDir + '/Coyote_Run_A.png' )
	  , Deadwood.resourceManager.getTexture( skinDir + '/Coyote_Run_B.png' )
	];
	this.deadTexture = Deadwood.resourceManager.getTexture( skinDir + '/coyote_Dead.png' );

	this.isDead = false;
	this.frameCount = 0;
	this.currentFrame = 0;
	this.waitTimer = 100;
	this.isMoving = false;
	this.moveSpeed = 1;
	this.animSpeed = 15;
	
	this.sprite = new PIXI.Sprite( this.idleTexture );
	this.sprite.position = position.clone();
	this.targetPosition = this.sprite.position.clone();
	this.sprite.anchor.x = 0.5;
}

Deadwood.GameObject.factories[ 'coyote' ] = function( position ) {
    var result = new Deadwood.Coyote( position );
	result.sprite.position = position;
	return result;
}

Deadwood.Coyote.prototype = Object.create( Deadwood.GameObject.prototype );

Deadwood.Coyote.prototype.interact = function( player ) {
	if( this.isDead ) {
		player.addFood( 20 );
		Deadwood.game.remove( this );
	}
}

Deadwood.Coyote.prototype.update = function() {
	Deadwood.GameObject.prototype.update.call( this );
	if( !this.isDead ) {
		if( !this.isMoving && this.waitTimer-- <= 0 ) {
			var deltaX = (Math.random() * Deadwood.World.stageSize.x) - ( Deadwood.World.stageSize.x/2);
			var deltaY = (Math.random() * Deadwood.World.stageSize.y) - ( Deadwood.World.stageSize.y/2);
			this.targetPosition = this.sprite.position.plusNew( new PIXI.Point( deltaX, deltaY ) );
			this.isMoving = true;
			this.waitTimer = Math.floor( Math.random() * 300 + 50 );
		}

		if( this.isMoving ) {
			var frameNum = Math.floor( this.frameCount++ / this.animSpeed ) % (this.runTextures.length);
			if( frameNum != this.currentFrame ) {
				this.currentFrame = frameNum;
				this.sprite.setTexture( this.runTextures[ frameNum ] );			
			}

			if( this.targetPosition.x > this.sprite.position.x ) {
				this.sprite.scale.x = 1;
			}
			else {
				this.sprite.scale.x = -1;
			}

			var diff = this.targetPosition.minusNew( this.sprite.position );
			if( diff.magnitudeSquared() <= 1 ) {
				this.isMoving = false;
				this.frameCount = 0;
				this.targetPosition = this.sprite.position;
				this.sprite.setTexture( this.idleTexture );
			}
			else {
				diff.normalise();
				this.move( diff.multiplyEq( this.moveSpeed ) );
			}
		}
	}
}

Deadwood.Coyote.prototype.kill = function() {
	this.isDead = true;
	this.frameCounter = 0;
	this.sprite.setTexture( this.deadTexture );
	this.nocollide = true;
	this.setInteractable( 'img/ui/harvest.png', 'img/ui/harvest_hi.png');
}

Deadwood.Coyote.prototype.onHit = function( projectile ) {
	this.kill();
}