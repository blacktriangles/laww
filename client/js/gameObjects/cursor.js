
Deadwood.Cursor = function( position ) {
	Deadwood.GameObject.call( this, position );
	this.renderLayer = RenderLayer.CURSOR;
	this.nocollide = true;
    this.sprite = PIXI.Sprite.createSprite( 'img/ui/crosshair.png', position );
    this.sprite.anchor.x = 0.5;
    this.sprite.anchor.y = 0.5;
}

Deadwood.Cursor.prototype = Object.create( Deadwood.GameObject.prototype );

Deadwood.Cursor.prototype.update = function() {
	Deadwood.GameObject.prototype.update.call( this );
	this.sprite.position = Deadwood.game.mousePos.clone();
}


