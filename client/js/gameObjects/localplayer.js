
Deadwood.LocalPlayer = function( id, position ) {
	Deadwood.BasePlayer.call( this, id, position );

    var cursor = new Deadwood.Cursor( new PIXI.Point( 0,0 ) );
	Deadwood.game.cursor = cursor;
    Deadwood.game.add( cursor );

	this.isLocalPlayer = true;

	var _this = this;
	$(window).keydown( function( ev ) {
		_this.onKeyDown( ev );
	});

	$(window).keyup( function( ev ) {
		_this.onKeyUp( ev );
	});

	$('#view').on( 'mousedown', function( ev ) {
        _this.onMouseDown( ev );
    });

    $('#view').on( 'mouseup', function( ev ) {
        _this.onMouseUp( ev );
    });
}

Deadwood.LocalPlayer.prototype = Object.create( Deadwood.BasePlayer.prototype );

Deadwood.LocalPlayer.prototype.update = function() {
	Deadwood.BasePlayer.prototype.update.call( this );

	// movement //
	var deltaMove = new PIXI.Point( 0, 0 );
    if(!this.reloading) {
        if (Deadwood.game.keyStates.getKeyState(87)) {
            deltaMove.y = -this.moveSpeed;
        }
        else if (Deadwood.game.keyStates.getKeyState(83)) {
            deltaMove.y = this.moveSpeed;
        }

        if (Deadwood.game.keyStates.getKeyState(65)) {
            deltaMove.x = -this.moveSpeed;
        }
        else if (Deadwood.game.keyStates.getKeyState(68)) {
            deltaMove.x = this.moveSpeed;
        }
    }

	// aim //
	this.aimPos = Deadwood.game.mousePos.minusNew( Deadwood.game.sprite.position );	

	// scrolling //
	var screenPos = this.sprite.position.plusNew( Deadwood.game.sprite.position );
	Deadwood.game.world.updateCellPos( this.cellPos );


    // are we free moving within an area of the screen, or moving the screen with the player?
    if( Deadwood.World.alwaysCenterPlayer == false ) {
        var delta = 0;
        if( screenPos.x < Deadwood.World.moveBounds.min.x ) {
            delta = Deadwood.World.moveBounds.min.x - screenPos.x;
        }
        else if( screenPos.x > Deadwood.World.moveBounds.max.x ) {
            delta = Deadwood.World.moveBounds.max.x - screenPos.x;
        }

        Deadwood.game.sprite.position.x += delta;

        delta = 0;
        if( Deadwood.World.moveBounds !== null ) {
            if( screenPos.y < Deadwood.World.moveBounds.min.y ) {
                delta = Deadwood.World.moveBounds.min.y - screenPos.y;
            }
            else if( screenPos.y > Deadwood.World.moveBounds.max.y ) {
                delta = Deadwood.World.moveBounds.max.y - screenPos.y;
            }
        }

        Deadwood.game.sprite.position.y += delta;
    }
    else {
        Deadwood.game.sprite.position.minusEq( deltaMove );
    }

    // move //
    this.move(deltaMove);
}

Deadwood.LocalPlayer.prototype.onKeyDown = function( ev ) {
    if( this.isDead ) return;
    if(this.reloading){

    }
    else {
        switch( ev.which ) {
            case 32:
                this.cycleWeapons();
                break;

            case 82:
                // weapon //
                //this.getCurrentWeapon().reload();
                this.showReloadOverlay()
                break;

            default:
//			console.log( ev.which );
                break;
        }
    }
}

Deadwood.LocalPlayer.prototype.onKeyUp = function( ev ) {
    if( this.isDead ) return;
    if(this.reloading){
        switch( ev.which ) {
            case 82:
                // weapon //
                this.hideReloadOverlay()
                break;
        }
    }
    else {

    }
}

Deadwood.LocalPlayer.prototype.showReloadOverlay = function( ) {
    this.reloading = true;
    var currentWeapon = this.getCurrentWeapon();
    currentWeapon.showReloadOverlay();
}

Deadwood.LocalPlayer.prototype.hideReloadOverlay = function( ) {
    this.reloading = false;
    var currentWeapon = this.getCurrentWeapon();
    currentWeapon.hideReloadOverlay();
}

Deadwood.LocalPlayer.prototype.onMouseDown = function( ev ) {
	if( this.isDead ) return;
	console.log( ev.button );
	var currentWeapon = this.getCurrentWeapon();
    if(this.reloading){
        currentWeapon.reloadClick(ev);
    }
    else {
        switch( ev.button ) {
            case 0:
                currentWeapon.trigger();
                break;

            case 2:
                currentWeapon.cock();
                break;
        }
    }

	ev.preventDefault();
}

Deadwood.LocalPlayer.prototype.onMouseUp = function( ev ) {
	if( this.isDead ) return;
}