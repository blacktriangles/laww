
Deadwood.RemotePlayer = function( id, position ) {
	Deadwood.BasePlayer.call( this, id, position );
	this.targetPos = position.clone();	
	this.smoothAnimCounter = 0;
}

Deadwood.RemotePlayer.prototype = Object.create( Deadwood.BasePlayer.prototype );

Deadwood.RemotePlayer.prototype.update = function() {
	if( this.isDead ) return;
	Deadwood.BasePlayer.prototype.update.call( this );

	var deltaMove = new PIXI.Point( 0, 0 );
	var diff = this.targetPos.minusNew( this.sprite.position );

	var forceMoveAnim = undefined;
	if( diff.magnitudeSquared() > this.moveSpeed ) {
		diff.normalise();
		deltaMove = diff.multiplyEq( this.moveSpeed );
		this.smoothAnimCounter = Deadwood.networkManager.remoteAnimSmoothFactor;
	}
	else if( this.smoothAnimCounter > 0 ) {
		this.smoothAnimCounter--;
		forceMoveAnim = this.deltaMove;
	}

	this.move( deltaMove, forceMoveAnim );
}

Deadwood.RemotePlayer.prototype.handleUpdatePacket = function( packet ) {
	if( this.isDead ) return;
	this.targetPos = new PIXI.Point( packet.position.x, packet.position.y );
	this.aimPos = packet.aimPos;
	if( this.weaponIndex != packet.selectedWeaponIndex ) {
		this.equipWeapon( packet.selectedWeaponIndex );
	}
}