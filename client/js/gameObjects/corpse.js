
Deadwood.Corpse = function( position ) {
    Deadwood.GameObject.call( this, position );
    this.renderLayer = RenderLayer.PROP;
    this.sprite = PIXI.Sprite.createSprite( 'img/characters/cowboy/corpse.png', position.clone() );
    this.fruited = true;
    this.isMovable = false;
    
    this.setInteractable( 'img/ui/harvest.png', 'img/ui/harvest_hi.png');
}

Deadwood.GameObject.factories[ 'corpse' ] = function( position ) {
	console.log( 'create corpse' );
    var result = new Deadwood.Corpse( position );
    return result;
}

Deadwood.Corpse.prototype = Object.create( Deadwood.GameObject.prototype );

Deadwood.Corpse.prototype.interact = function( player ) {
}

Deadwood.Corpse.prototype.onHit = function( projectile ) {
}

Deadwood.Corpse.prototype.update = function() {
    Deadwood.GameObject.prototype.update.call( this );
}