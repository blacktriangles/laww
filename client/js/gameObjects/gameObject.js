
Deadwood.GameObject = function( position ) {
	this.isInteractable = false;
	this.renderLayer = RenderLayer.PLAYER;
	this.isMovable = true;
	this.sprite = null;
	this.cellPos = Deadwood.World.getCellPosFromWorldPos( position );
}

Deadwood.GameObject.factories = {};

Deadwood.GameObject.create = function( type, position ) {
	return Deadwood.GameObject.factories[ type ]( position );
}

Deadwood.GameObject.prototype.setInteractable = function( interactIcon, interactIconHi ) {
	this.isInteractable = true;
	this.isHovering = false;
	this.isInRange = false;

	this.interactTexture = Deadwood.resourceManager.getTexture( interactIcon );
	this.interactHiTexture = Deadwood.resourceManager.getTexture( interactIconHi );

	this.interactIcon = new PIXI.Sprite( this.interactTexture );
	this.interactIcon.position = this.sprite.position.plusNew( new PIXI.Point( 0, -16 ) );
	Deadwood.game.renderLayers[ RenderLayer.OVERLAY ].addChild( this.interactIcon );
	this.interactIcon.visible = false;
}

Deadwood.GameObject.prototype.onDestroy = function() {
	if( this.isInteractable ) {
		Deadwood.game.renderLayers[ RenderLayer.OVERLAY ].removeChild( this.interactIcon );
		this.interactIcon = null;
	}
}

Deadwood.GameObject.prototype.update = function() {
	if( this.isInteractable ) {
		var localPlayer = Deadwood.game.localPlayer;
		if( Vector2.distanceSquared( this.sprite.position, localPlayer.sprite.position ) < localPlayer.getCurrentWeapon().getInteractRangeSqr() ) {
			if( this.isInRange == false ) {
				this.isInRange = true;
				this.interactIcon.visible = true;
			}

			if( this.sprite.isMouseOver() ) {
				if( this.isHovering == false ) {
					this.isHovering = true;
					this.interactIcon.setTexture( this.interactHiTexture );
				}
			}
			else {
				if( this.isHovering == true ) {
					this.isHovering = false;
					this.interactIcon.setTexture( this.interactTexture );
				}	
			}

		}
		else if( this.isInRange == true ) {
			this.isInRange = false;
			this.interactIcon.visible = false;
		}
	}
	else if( this.interactIcon !== null && this.interactIcon !== undefined ) {
		this.interactIcon.visible = false;
		this.isInRange = false;
		this.isHovering = false;
	}
}

Deadwood.GameObject.prototype.move = function( deltaMove ) {
	if( this.isMovable == false || this.sprite == null ) return;

	this.sprite.position.plusEq( deltaMove );
	var newCellPos = Deadwood.World.getCellPosFromWorldPos( this.sprite.position );
	if( newCellPos.equals( this.cellPos ) == false ) {
		Deadwood.game.world.changeCell( this, this.cellPos, newCellPos );
		this.cellPos = newCellPos;
	}
}

Deadwood.GameObject.prototype.destroy = function() {
	Deadwood.game.remove( this );
}

Deadwood.GameObject.prototype.handleUpdatePacket = function( packet ) {
	console.log( 'GameObject taking update packet: ' + JSON.stringify( packet ) );
}