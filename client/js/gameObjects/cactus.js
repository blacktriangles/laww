
Deadwood.Cactus = function( position ) {
    Deadwood.GameObject.call( this, position );
    this.renderLayer = RenderLayer.PROP;
    this.sprite = PIXI.Sprite.createSprite( 'img/environment/desert/cactus_fruit.png', position.clone() );
    this.fruited = true;
    this.isMovable = false;
    
    this.setInteractable( 'img/ui/harvest.png', 'img/ui/harvest_hi.png');
}

Deadwood.GameObject.factories[ 'cactus' ] = function( position ) {
    var result = new Deadwood.Cactus( position );
    return result;
}

Deadwood.Cactus.prototype = Object.create( Deadwood.GameObject.prototype );

Deadwood.Cactus.prototype.interact = function( player ) {
    player.addFood( 10 );
    this.removeFruit();
}

Deadwood.Cactus.prototype.removeFruit = function() {
    this.fruited = false;
    this.isInteractable = false;
    this.sprite.changeTexture('img/environment/desert/cactus_nofruit.png');
    var _this = this;
    window.setTimeout(function(){
        _this.fruited = true;
        _this.isInteractable = true;
        _this.sprite.changeTexture('img/environment/desert/cactus_fruit.png');
    },30000)
}

Deadwood.Cactus.prototype.onHit = function( projectile ) {
    this.removeFruit();
}

Deadwood.Cactus.prototype.update = function() {
    Deadwood.GameObject.prototype.update.call( this );
}