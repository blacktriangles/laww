
Deadwood.Projectile = function( packet ) {
	packet.position = new PIXI.Point( packet.position.x, packet.position.y );
	packet.dir = new PIXI.Point( packet.dir.x, packet.dir.y );

	Deadwood.GameObject.call( this, packet.position );
	this.id = packet.id;
	this.offset = packet.dir.multiplyNew( 15 ).plusEq( new PIXI.Point( 0, 5 ) );
	this.path = packet.img;
	this.sprite = PIXI.Sprite.createSprite( this.path, packet.position.plusNew( this.offset ) );
	this.sprite.anchor.x = 0.5;
	this.sprite.anchor.y = 0.5;
	this.startPos = packet.position.clone();
	this.owner = Deadwood.game.world.getPlayer( packet.shooterId );
	this.dir = packet.dir.clone();
	this.dir.normalise();
	this.speed = packet.speed;
	this.range = packet.range;
	this.hitRadiusSqr = packet.hitRadiusSqr;

	this.deltaMove = this.dir.multiplyNew( this.speed );
	this.sprite.rotation = this.deltaMove.angle( true ) + 3.14;
}

Deadwood.Projectile.prototype = Object.create( Deadwood.GameObject.prototype );

Deadwood.Projectile.prototype.update = function() {
	Deadwood.GameObject.prototype.update.call( this );

	this.sprite.position.plusEq( this.deltaMove );

	for( var id in Deadwood.game.world.players ) {
		if( id != this.owner.id ) {
			var player = Deadwood.game.world.players[id];
			if( player !== null && player !== undefined	) {
				var dist2 = PIXI.Point.distanceSquared( player.sprite.position, this.sprite.position );
				if( dist2 < this.hitRadiusSqr ) {
					Deadwood.game.removeProjectile( this.id );
					return;
				}	
			}
		}
	}
}