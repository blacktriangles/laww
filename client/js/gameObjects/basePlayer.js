
Deadwood.BasePlayer = function( 
	  	id
	  , position
	  , weapons
	  , textures 
	) 
{
	Deadwood.GameObject.call( this, position );

	this.isLocalPlayer = false;
	this.isDead = false;

	// static info //
	this.weapons = weapons || [
			new Deadwood.WeaponFist( this )
		  , new Deadwood.WeaponRevolver( this )
		];
	
	var skinDir = 'img/characters/cowboy';
	this.textures = textures || {
		idle: Deadwood.resourceManager.getTexture( skinDir + '/cowboy.png' )
	  , walk: [
			Deadwood.resourceManager.getTexture( skinDir + '/walk1.png' )
		  , Deadwood.resourceManager.getTexture( skinDir + '/walk2.png' )
		  , Deadwood.resourceManager.getTexture( skinDir + '/walk3.png' )
		  , Deadwood.resourceManager.getTexture( skinDir + '/walk4.png' )
		  , Deadwood.resourceManager.getTexture( skinDir + '/walk5.png' )
		  , Deadwood.resourceManager.getTexture( skinDir + '/walk6.png' )
		]
	};

	this.moveSpeed = 1;

	// base info //
	this.id = id;
	this.animSpeed = 10;
	this.aimPos = new PIXI.Point( 0, 0 );

	// weapons //
	this.selectedWeapon = 0;
	var currentWeapon = this.weapons[ this.selectedWeapon ];

	// state //
	this.isMoving = false;
	this.animCounter = 0;
	this.currentFrame = 0;
	
	// initailization //
	this.sprite = new PIXI.DisplayObjectContainer();
	this.sprite.position = position.clone();

	this.weaponSprite = PIXI.Sprite.createSprite( currentWeapon.skin, currentWeapon.offset );
	this.weaponSprite.anchor = currentWeapon.anchor;
	this.sprite.addChild( this.weaponSprite );

	this.bodySprite = new PIXI.Sprite( this.textures.idle );
	this.bodySprite.anchor.x = 0.5;
	this.sprite.addChild( this.bodySprite );
}

Deadwood.BasePlayer.prototype = Object.create( Deadwood.GameObject.prototype );

// public methods /////////////////////////////////////////////////////////////
Deadwood.BasePlayer.prototype.getCurrentWeapon = function() {
	return this.weapons[ this.selectedWeapon ];
}

Deadwood.BasePlayer.prototype.equipWeapon = function( weapon ) {
	// if a number was passed, use it like an index
	if( typeof weapon == "number" ) {
		this.weaponIndex = weapon;
		weapon = this.weapons[ this.weaponIndex ];
	}

	this.weaponSprite.changeTexture( weapon.skin );	
	this.weaponSprite.anchor = weapon.anchor;
	this.weaponSprite.position = weapon.offset;
}

Deadwood.BasePlayer.prototype.cycleWeapons = function() {
	if( this.selectedWeapon++ >= this.weapons.length-1 )
		this.selectedWeapon = 0;

	this.equipWeapon( this.getCurrentWeapon() );
}

Deadwood.BasePlayer.prototype.addFood = function(amount) {
    console.log("player at "+amount+" food");
}

Deadwood.BasePlayer.prototype.onHit = function() {
	this.isDead = true;
	Deadwood.game.world.removePlayer( this.id );
}

// overridable methods ////////////////////////////////////////////////////////
Deadwood.BasePlayer.prototype.handleUpdatePacket = function( packet ) {}

Deadwood.BasePlayer.prototype.update = function() {
	if( this.isDead ) return;
	Deadwood.GameObject.prototype.update.call( this );

	// weapon update //
	this.getCurrentWeapon().update();
}

Deadwood.BasePlayer.prototype.move = function( deltaMove, forceMoveAnim ) {
	if( this.isDead ) return;
	Deadwood.GameObject.prototype.move.call( this, deltaMove );
	
	// facing //
	var flip = this.aimPos.x > this.sprite.position.x;
	if( flip )
	{
		this.sprite.scale.x = -1;
	}
	else
	{
		this.sprite.scale.x = 1;
	}

	forceMoveAnim = forceMoveAnim || deltaMove;

	if( forceMoveAnim.x != 0 || forceMoveAnim.y != 0 ) {
		this.isMoving = true;

		var movingRight = forceMoveAnim.x > 0;
		var facingRight = flip;

		var frameNum = Math.floor( this.animCounter++ / this.animSpeed ) % ( this.textures.walk.length );
		if( movingRight != facingRight ) {
			frameNum = this.textures.walk.length - frameNum - 1;
		}

		if( frameNum != this.currentFrame ) {
			this.currentFrame = frameNum;
			this.bodySprite.setTexture( this.textures.walk[this.currentFrame] );
		}
	}
	else {
		this.currentFrame = 0;
		this.animCounter = 0;
		this.isMoving = false;
		this.bodySprite.setTexture( this.textures.idle );
	}

	var toCursor = this.sprite.position.minusNew( this.aimPos );
	this.weaponSprite.rotation = (toCursor.angle( true ) + ( flip ? 3.14 : 0 )) * ( flip ? -1 : 1 );

}