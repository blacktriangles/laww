
///###hsmith $WARNING this packet is used as a part of many other packets, and as the player data
/// storage on the server, so be wary about what you add to it exactly.

var Deadwood = Deadwood || {};
Deadwood.Packets = Deadwood.Packets || {};

Deadwood.Packets.UpdatePlayer = function( 
		playerId
	  , position
	  , aimPos
	  , selectedWeaponIndex
	)
{
	this.id = playerId;
	this.position = position;
	this.aimPos = aimPos;
	this.selectedWeaponIndex = selectedWeaponIndex
}

Deadwood.Packets.UpdatePlayer.prototype.equals = function( rhs ) {
	if( rhs === null || rhs === undefined ) return false;

	return !( this.id != rhs.id
		|| this.position.x != rhs.position.x
	 	|| this.position.y != rhs.position.y
		|| this.aimPos.x != rhs.aimPos.x
		|| this.aimPos.y != rhs.aimPos.y 
		|| this.selectedWeaponIndex != rhs.selectedWeaponIndex );
}


var module = module || { exports: 0 };
module.exports = Deadwood.Packets.UpdatePlayer;