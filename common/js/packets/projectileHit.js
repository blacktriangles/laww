
var Deadwood = Deadwood || {};
Deadwood.Packets = Deadwood.Packets || {};

Deadwood.Packets.ProjectileHit = function( projId, playerId ) {
	this.projectileId = projId;
	this.playerId = playerId;
}


var module = module || { exports: 0 };
module.exports = Deadwood.Packets.ProjectileHit;