
var Deadwood = Deadwood || {};
Deadwood.Packets = Deadwood.Packets || {};

Deadwood.Packets.SpawnProjectile = function(
		img
	  , shooterId
	  , dir
	  , pos
	  , speed
	  , range
	  , hitRadius
	)
{
	this.id = null;
	this.img = img;
	this.shooterId = shooterId;
	this.dir = dir;
	this.position = pos;
	this.speed = speed;
	this.range = range;
	this.hitRadiusSqr = hitRadius * hitRadius;
}

var module = module || { exports: 0 };
module.exports = Deadwood.Packets.SpawnProjectile;