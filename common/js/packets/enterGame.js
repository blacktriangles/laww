
var Deadwood = Deadwood || {};
Deadwood.Packets = Deadwood.Packets || {};

Deadwood.Packets.EnterGame = function( newPlayerData, poi ) {
	this.localPlayer = newPlayerData;
	this.remotePlayers = poi;
}


var module = module || { exports: 0 };
module.exports = Deadwood.Packets.EnterGame;