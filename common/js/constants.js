
// pixi ///////////////////////////////////////////////////////////////////////
var Deadwood = Deadwood || {};
Deadwood.Point = Deadwood.Point || require( App.dirs.common + 'lib/vector2' );

// network //////////////////////////////////////////////////////////////////////
Deadwood.NetworkManager = Deadwood.NetworkManager || {};
Deadwood.NetworkManager.updateFrequency	= 0;
Deadwood.NetworkManager.remoteAnimSmoothFactor = 5;

// world //////////////////////////////////////////////////////////////////////
Deadwood.World = Deadwood.World || {};

Deadwood.World.tileToPixelPos = function( tilePos ) {
	return tilePos.multiplyNewVec( Deadwood.World.tileSize );
}

Deadwood.World.pixelToTilePos = function( pixelPos ) {
	return new Deadwood.Point( Math.floor( pixelPos.x / Deadwood.World.tileSize.x )
						  , Math.floor( pixelPos.y / Deadwood.World.tileSize.y ) );
}

Deadwood.World.getCellPosFromTilePos = function( tilePos ) {
	return new Deadwood.Point( Math.floor( tilePos.x / Deadwood.Cell.cellDims.x )
						  , Math.floor( tilePos.y / Deadwood.Cell.cellDims.y ) );
}

Deadwood.World.getCellPosFromWorldPos = function( worldPos ) {
	var tilePos = Deadwood.World.pixelToTilePos( worldPos );
	return Deadwood.World.getCellPosFromTilePos( tilePos );
}

Deadwood.World.scaleFactor = new Deadwood.Point( 2.5, 2.5 );
Deadwood.World.tileSize = new Deadwood.Point( 32, 32 );
Deadwood.World.viewSize = new Deadwood.Point( 16, 9 );
Deadwood.World.stageSize = Deadwood.World.tileToPixelPos( Deadwood.World.viewSize );
Deadwood.World.centerStage = Deadwood.World.stageSize.divideNew( 2 );
Deadwood.World.stageBorder = new Deadwood.Point( 50, 50 );
Deadwood.World.alwaysCenterPlayer = true;
Deadwood.World.moveBounds = Deadwood.World.alwaysCenterPlayer ? null : {
	min: Deadwood.World.stageBorder.clone()
  , max: Deadwood.World.stageSize.minusNew( Deadwood.World.stageBorder )
}

// cell ///////////////////////////////////////////////////////////////////////
Deadwood.Cell = Deadwood.Cell || {};
Deadwood.Cell.cellDims = new Deadwood.Point( 20, 20 );
Deadwood.Cell.pixelDims = Deadwood.World.tileToPixelPos( Deadwood.Cell.cellDims );

Deadwood.Cell.cellToWorldPixelPos = function( cellpos ) {
	return Deadwood.Cell.pixelDims.multiplyNewVec( cellpos );
}

Deadwood.Cell.localTileToWorldPos = function( cellpos, localTilePos ) {
	var localPixelPos = Deadwood.World.tileToPixelPos( localTilePos );
	return localPixelPos.plusEq( Deadwood.Cell.cellToWorldPixelPos( cellpos ) );
}

Deadwood.Cell.randomPixelInCellGlobal = function( cellpos ) {
	var randomLocalPixel = Deadwood.Cell.randomPixelInCellLocal();
	return Deadwood.Cell.cellToWorldPixelPos( cellpos );
}

Deadwood.Cell.randomTileInCellGlobal = function( cellpos ) {
	var randomLocalTile = Deadwood.Cell.randomTileInCellLocal();
	var result = (new Deadwood.Point( posx, posy )).plusEq( cellpos );
	return result;	
}

Deadwood.Cell.randomPixelInCellLocal = function() {
	var posx = Math.random() * Deadwood.Cell.pixelDims.x;
	var posy = Math.random() * Deadwood.Cell.pixelDims.y;
	return new Deadwood.Point( posx, posy );
}

Deadwood.Cell.randomTileInCellLocal = function() {
	var posx = Math.floor( Math.random() * Deadwood.Cell.cellDims.x );
	var posy = Math.floor( Math.random() * Deadwood.Cell.cellDims.y );
	return new Deadwood.Point( posx, posy );
}

module.exports = Deadwood;