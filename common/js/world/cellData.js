
var Deadwood = Deadwood || {};

Deadwood.CellData = function( pos, seed ) {
	this.cellPosition = pos;
	this.seed = seed;
	this.objects = [];
}

Deadwood.CellData.ObjectData = function( id, position ) {
	this.id = id;
	this.position = position;
}


module.exports = Deadwood.CellData;