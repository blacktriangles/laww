
var Deadwood = Deadwood || {};

Deadwood.Cell = function( pos ) {
	this.data = new Deadwood.CellData( pos );
	this.worldTilePos = this.data.cellPosition.multiplyNewVec( Deadwood.Cell.cellDims );
	this.worldPos = Deadwood.World.tileToPixelPos( this.worldTilePos );
}

Deadwood.Cell.localTileToWorldTile = function( localTilePos ) {
	return localTilePos.plusNew( this.worldTilePos );
}

Deadwood.Cell.localPixelToWorldPixel = function( localPixelPos ) {
	return localPixelPos.plusNew( this.worldPos )
}


module.exports = Deadwood.Cell;